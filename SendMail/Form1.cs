﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SendMail
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        Attachment attachment = null;
        private void btnAttach_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtFile.Text = openFileDialog.FileName;
            }
        }
        /// <summary>
        /// Gửi mail theo các thông tin truyền vào
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="message"></param>
        /// <param name="file"></param>
        void GuiMail(string from, string to, string subject, string message, Attachment file = null)
        {
            MailMessage mess = new MailMessage(from, to, subject, message);

            if (attachment != null)
            {
                mess.Attachments.Add(attachment);
            }


            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.EnableSsl = true;

            client.Credentials = new NetworkCredential(txtUser.Text, txtPassword.Text);

            client.Send(mess);
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() =>
           {
               attachment = null;
               try
               {
                   FileInfo fileInfo = new FileInfo(txtFile.Text);
                   attachment = new Attachment(txtFile.Text);
               }
               catch (Exception)
               {

                   throw;
               }

               StreamReader reader = new StreamReader(txtTo.Text);
               string email = reader.ReadLine();
               while (email != null)
               {
                   GuiMail(txtUser.Text, email, txtSubject.Text, txtMessage.Text, attachment);
                   email = reader.ReadLine();
               }
               reader.Close();
           });
            thread.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                txtTo.Text = openFileDialog.FileName;
            }
        }
    }
}
